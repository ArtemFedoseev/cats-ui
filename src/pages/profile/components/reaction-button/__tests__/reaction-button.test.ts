import { test, expect } from '@playwright/test';

const selector = '//button[@title="Лайкнуть"]';

test('Кнопка постановки лайка кликабельная', async ({ page }) => {
  await page.goto(
    '/iframe.html?args=&id=reactionbutton--default&viewMode=story'
  );
  await expect(page.locator(selector)).toBeEnabled();
});
