import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
/**
 * Класс для реализации логики со страницей рейтинга.
 *
 */
export class RatingPage {
  private page: Page;
  public errorSelector: string;
  public errorMessage: string;
  public tableSelector: string;
  public rowsSelector: string;
  public columnLocator: string;

  constructor({
    page,
   }: {
    page: Page;
  }) {
    this.page = page;
    this.errorSelector = '.ajs-error';
    this.tableSelector = '.rating-names_table__Jr5Mf';
    this.errorMessage = 'Ошибка загрузки рейтинга';
    this.rowsSelector = 'tr';
    this.columnLocator = 'td';
  }

  async openRatingPage() {
    return await test.step('Открываю страницу рейтинга', async () => {
      await this.page.goto('/rating')
    })
  }
}

export type RatingPageFixture = TestFixture<
  RatingPage,
  {
    page: Page;
  }
  >;

export const ratingPageFixture: RatingPageFixture = async (
  { page },
  use
) => {
  const ratingPage = new RatingPage({ page });

  await use(ratingPage);
};

