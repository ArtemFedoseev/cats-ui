import { test as base, expect } from '@playwright/test';
import { serverErrorResponse } from '../__mocks__/bad-response';
import { RatingPage, ratingPageFixture } from '../__page-object__';

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе rating - отображается попап ошибки', async ({
  page,
  ratingPage,
}) => {
  //Замокать ответ метода получения рейтинга ошибкой на стороне сервера
  await page.route(
    request => request.href.includes('/api/likes/cats/rating'),
    async route => {
      await route.fulfill({
        status: 500,
        contentType: 'application/json',
        body: JSON.stringify(serverErrorResponse),
      });
    }
  );
  //Перейти на страницу рейтинга
  await ratingPage.openRatingPage();
  //Проверить, что отображается текст ошибка загрузки рейтинга
  await expect(page.locator(ratingPage.errorSelector)).toHaveText(
    ratingPage.errorMessage
  );
});

test('Рейтинг котиков отображается по убыванию', async ({
  page,
  ratingPage,
}) => {
  //Перейти на страницу рейтинга
  await ratingPage.openRatingPage();
  const tableLocator = page.locator(ratingPage.tableSelector).first();
  //Достать необходимые значения
  const rows = tableLocator.locator(ratingPage.rowsSelector);
  const dataString: string[] = [];

  // Перебираем строки рейтинга и последнее значение (число лайков) помещаещаем в dataString
  const rowCount = await rows.count();
  for (let i = 0; i < rowCount; i++) {
    const row = rows.nth(i);
    const lastCell = row.locator(ratingPage.columnLocator).last();
    const cellText = await lastCell.innerText();
    dataString.push(cellText);
  }

  // Преобразуем в числа, добавляем массив сортированных значений и сравниваем
  let dataNumber = dataString.map(Number);
  let sortedData = dataNumber.map(Number).sort((a, b) => b - a);

  await expect(dataNumber).toEqual(sortedData);
});
