export const serverErrorResponse = {
  data: null,
  isBoom: true,
  isServer: false,
  output: {
    statusCode: 500,
    payload: {
      statusCode: 500,
      error: 'Internal Server Error',
    },
    headers: {},
  },
};
